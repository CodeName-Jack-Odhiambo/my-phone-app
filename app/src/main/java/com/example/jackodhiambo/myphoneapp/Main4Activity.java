package com.example.jackodhiambo.myphoneapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main4Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
    }
    public void toHome(View view)
    {
        Intent fourthtActivity= new Intent(Main4Activity.this, MainActivity.class);
        startActivity(fourthtActivity);

    }
}
