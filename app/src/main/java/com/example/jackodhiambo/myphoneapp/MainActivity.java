package com.example.jackodhiambo.myphoneapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void toMyMusic(View view)
    {
        Intent firstActivity= new Intent(MainActivity.this, Main2Activity.class);
        startActivity(firstActivity);
    }

    public void toMySport(View view)
    {
        Intent secondActivity= new Intent(MainActivity.this, Main3Activity.class);
        startActivity(secondActivity);
    }

    public void toMySchedule(View view)
    {
        Intent thirdActivity= new Intent(MainActivity.this, Main4Activity.class);
        startActivity(thirdActivity);
    }
}
